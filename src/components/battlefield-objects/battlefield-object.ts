import {GameUtils} from '../../utils/game-utils.ts';
import {Ammo} from './ammo.ts';
import {Player} from '../player/player.ts';

export class BattlefieldObject extends Phaser.Sprite {
    public playerId:number;
    public ammoGroup;
    //public enemyId:number;
    private life:number;
	//public sprite:Phaser.Sprite;
	private gameState:Phaser.Game;
	private speed:number = 100;
	private hitArray = [];
	private currentEnemy = null;
	private rangeAttackStatus:boolean = true;
	private hitTime:number = 0;
	private nextRangedFire = 0;
	private nextFire = 0;
	private fireRate = 3000;
	private fireRateRanged = 3000;
	private ammo = [];
	private enemyTarg:Phaser.Group;
	private targ:Phaser.Group;

	private movementSpeed = 50;
	private fighterPosition:number;

	//debug
	private counter = 0;

	//, playerId:number, enemyPlayer:Player
	constructor (gameState:Phaser.Game, startX:number, startY:number, spriteId:string) {
		super(gameState, startX, startY, spriteId);

		//playerId logic	
		/*
		this.fighterPosition = GameUtils.isPlayer1(playerId) ? 1 : -1;
		this.playerId = playerId;
		*/


		this.gameState = gameState; //DO I STILL NEED
		//this.enemyId = enemyId;
		this.name = this.key + '_' + String(GameUtils.createSpriteId());
		gameState.physics.arcade.enable(this);

		//enemyPlayer group logic
		/*
		this.targ = enemyPlayer.wallGroup;
		this.enemyTarg = enemyPlayer.fighterGroup; */

		this.events.onKilled.add(this.onKill, this);
		//Temporary conditional until proper config is put in place to setup whether sprite can attack/move/life etc.
		if (spriteId === 'wallpiece') {
			this.body.immovable = true;
			this.ammoGroup = this.gameState.add.group();
		} else {
			this.alive = true;
			this.health = 60;
			this.ammoGroup = this.gameState.add.group();
		    this.ammoGroup.enableBody = true;
		    this.ammoGroup.physicsBodyType = Phaser.Physics.ARCADE;
		    this.body.velocity.x = this.movementSpeed * this.fighterPosition;
		    this.createAmmo();
		}
		//console.log(this, 'what is this?');
	    //this.fighters[i].onUpdate(this.players[this.fighters[i].enemyId].wallGroup, this.players[this.fighters[i].enemyId].fighterGroup);
	};

	public config (playerId, enemyPlayer) {
		this.fighterPosition = GameUtils.isPlayer1(playerId) ? 1 : -1;
		this.playerId = playerId;

		this.targ = enemyPlayer.wallGroup;
		this.enemyTarg = enemyPlayer.fighterGroup;
	}

	private createAmmo () {
		var ammo = []
		for (let i = 0; i < 3; i ++ ) {
			let myAmmo = new Ammo(this.gameState, this.x + (80 * this.fighterPosition) , this.y, 'ammo', this.fighterPosition);
        	myAmmo.exists = false;
        	ammo.push(myAmmo);
        }
            //console.log(ammo, 'my ammo!')
        this.ammoGroup.addMultiple(ammo, false);
	};

	public update () {
		
		var wallHit = this.gameState.physics.arcade.overlap(this, this.targ, this.onHit, null, this);
		
		var enemyHit = this.gameState.physics.arcade.overlap(this, this.enemyTarg, this.onHit, null, this);

		this.gameState.physics.arcade.overlap(this.ammoGroup, this.enemyTarg, this.onAttack, null, this);

		this.gameState.physics.arcade.overlap(this.ammoGroup, this.targ, this.onAttack, null, this);

		if (!enemyHit && !wallHit) {
			//Temporary conditional until proper config is put in place to setup whether sprite can attack/move/life etc.
			if (this.key !== 'wallpiece') { // && this.body !== null
				this.body.velocity.x = this.movementSpeed * this.fighterPosition;
				this.rangeAttackStatus = true;
			}
		} else {
			
		}


		//NOW an object will be iterated through of all enemy groups which is passed in on init;
		//TEST that this holds up after adding new items to the group from game.ts using interval strat
		//if (this.alive) {
		//Temporary conditional until proper config is put in place to setup whether sprite can attack/move/life etc.
		if (this.key !== 'wallpiece') {
			this.attack();
		}
		//}
	};

	public attack() {
		//console.log(this.name, this.ammoGroup, 'WHAT IS MY AMMO?');
	    //Ranged attacks
	    if (this.gameState.time.now > this.nextRangedFire && this.ammoGroup.countDead() > 0 && this.rangeAttackStatus)
        {
            
            this.nextRangedFire = this.gameState.time.now + this.fireRateRanged;
           
            var bullet = this.ammoGroup.getFirstDead(false);
            bullet.reset(this.x + (80 * this.fighterPosition) , this.y);
            
            this.gameState.physics.arcade.moveToXY(bullet, 800 * this.fighterPosition, this.y, 120);
        }
	};

	public onHit (x, targ) {
		//if (x.body !== null && targ.body !== null) {
		//console.log(targ, 'what is my targ onHit');
			if (this.currentEnemy === null) {
				this.currentEnemy = targ;
			}

			//targ.kill();
			
			if (this.counter < 2 ) {
				//console.log(targ, 'what is this targ?');
				//console.log(x.parent.name + ':current sprite', targ.parent.name, ':targ overlapped', targ);
				
				this.counter++;

			}

			//console.log(groupType,'THIST MY TEST')
			x.body.velocity.x = 0;
			
			//x.body.velocity.x = 100 * (this.fighterPosition * -1);
			//console.log(targ.type,'this is sprite name');
			

			targ.body.velocity.x = 0;
			
			x.rangeAttackStatus = false;
			targ.rangeAttackStatus = false;

			//close by attack
	        if (this.gameState.time.now > this.nextFire && !this.rangeAttackStatus)
	        {
	        	//console.log('do i make it here?');
	            this.nextFire = this.gameState.time.now + this.fireRate;
	           	//this next block of logic is mimicking what should be done by changing to an attack frame of a spritesheet
	           	let myAmmo = new Ammo(this.gameState, this.x + (10 * this.fighterPosition) , this.y, 'wallpiece', this.fighterPosition);
	           	this.gameState.add.existing(myAmmo);
	           	//console.log(enemyTarg,  'enemyTarg')
	           	targ.damage(myAmmo.attackPoints);
	           	setTimeout(() => {
	           		myAmmo.kill();
	           	},1000);
	           	//myAmmo.exists = true;
	        }
		//}
		
	};

	public onCollide(x, targ) {
		//console.log(targ, 'what is my targ collide');
	};

	public onAttack(x, targ) {
		if (x.hitTime !== this.gameState.time.time) {
			x.hitTime = this.gameState.time.time;
			x.kill();
			//console.log(x, 'THISIS')
			//targ.ammoGroup.destroy();
			//console.log(targ.ammoGroup, targ.name, 'WHAT IS MY AMMO GROUP?'); 
			targ.damage(x.attackPoints);
			//console.log(targ.key, targ, 'what is this targ now?');
			if (!targ.alive) {
				//targ.ammoGroup = null; //.destroy();
				targ.destroy();
				///kill ammo group here after properly extending sprite
				//close by attacks will be dead once sprite sheets are used
				console.log('he dead');
			}		
		}
	};

	private onKill (e) {
		e.ammoGroup.destroy();
		e.destroy();
		//console.log( e,'YEO');
	}
}
