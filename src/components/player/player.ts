import * as Phaser from 'phaser';
import {GameUtils} from '../../utils/game-utils.ts';
import {BattlefieldObject} from '../battlefield-objects/battlefield-object.ts';

/*
var playerCounter = 0;

var Wall = function(life) {
  this.life = life;
}

var Player = function(game) {
    playerCounter++;
    this.id = playerCounter;
    this.createWallPieces(game);
};

Player.prototype.createWallPieces = function(game) {
    var xStart = (this.id === 1) ? 0 : game.game.width - game.cache.getImage("wallpiece").width;
    console.log(game,'my id');
    for (var i = 0; i < 8; i++)
    {
        //  This creates a new Phaser.Sprite instance within the group
        //  It will be randomly placed within the world and use the 'baddie' image to display
        let wallPiece = game.wallpieces.create(xStart, game.cache.getImage("wallpiece").height * i, 'wallpiece');
        wallPiece.onHit = this.onHit;
        wallPiece.body.immovable = true;
    }
};

Player.prototype.onHit = function(wall, hitter) {
    console.log(hitter);
    wall.destroy();
}*/

export class Player {
    public id:number; //0 left, 1 right
    //public wallpieces:any;
    public wallGroup:any;
    public fighterGroup:any;
    //public enemyWallGroup:any;
    //private enemyWallGroup:any;
    
    public enemyId:number;
    private gameState:Phaser.Game;
    //private enemyFighters:any;
    //private enemyFighters:
    constructor (id:number, gameState:Phaser.Game) {
        this.id = id;
  
        this.gameState = gameState;
        this.wallGroup =  <IBattlefieldGroup>gameState.add.group();
        this.wallGroup.name = 'wall:' + id;
        this.fighterGroup = <IBattlefieldGroup>gameState.add.group();
        this.fighterGroup.name = 'fighter:' + id;








        //this.wallpieces = <IBattlefieldGroup>gameState.add.group();
    this.wallGroup.enableBody = true;
    this.wallGroup.physicsBodyType = Phaser.Physics.ARCADE;
    this.wallGroup.battleFieldType = 'wall';

    this.fighterGroup.enableBody = true;
    this.fighterGroup.physicsBodyType = Phaser.Physics.ARCADE;
    this.fighterGroup.battleFieldType = 'fighter';



       // this.fighters = fighterGroup;



        //this.wallGroup.enableBody = true;
        //this.wallGroup.physicsBodyType = Phaser.Physics.ARCADE;

        /*this.wallpieces = this.add.group();
        this.wallpieces.enableBody = true;
        this.wallpieces.physicsBodyType = Phaser.Physics.ARCADE;*/

        //CENTRALIZE THIS

        //this.createWallPieces();
        
       // console.log('1234567');
    };

    public init (enemyId:number, players:Array<Player>) {
        this.setEnemyId(enemyId);
        this.createWallPieces(players[enemyId]);
    }

    /*
    public setEnemyWall(wallGroup) {
        this.enemyWallGroup = wallGroup;
    }*/

    public addFighter(fighter) {
        this.fighterGroup.add(fighter);
    }

    private setEnemyId(id:number) {
        this.enemyId = id;
    }

    /*
    public setEnemyFighters(fighters) {
        this.enemyFighters = fighters;
    }

    public getEnemyFighters():any{
        return this.enemyFighters;
    }

    public getEnemyWall() {
        console.log(this.enemyWallGroup, 'WTF, this.enemyWallGroup??');
        return this.enemyWallGroup;
    }*/

    private createWallPieces (enemyPlayer:Player) {
        var xStart = GameUtils.isPlayer1(this.id) ? 0 : this.gameState.width - this.gameState.cache.getImage("wallpiece").width;
        //console.log(game,'my id');
        for (let i = 0; i < 6; i++)
        {
            //  This creates a new Phaser.Sprite instance within the group
            //  It will be randomly placed within the world and use the 'baddie' image to display
            //let wallPiece = this.wallGroup.create(xStart, this.gameState.game.cache.getImage("wallpiece").height * i, 'wallpiece');
            let wallPiece = new BattlefieldObject(this.gameState, xStart, this.gameState.cache.getImage("wallpiece").height * i, 'wallpiece', this.id, enemyPlayer);
            //wallPiece.body.immovable = true;

            wallPiece.playerId = this.id;
            this.wallGroup.add(wallPiece);
            


            //this.wallGroup.create()
            //console.log(wallPiece,'what is my wallpiece???');
            //wallPiece.onHit = this.onHit;
            
            //console.log(wallPiece, 'wallPiece');
            //this.wallGroup.add(wallPiece);
        }
         
         //console.log(this.wallGroup,'WHAT IS MY WALL GROUP')
    };


}