/// <reference path="../lib/index.d.ts"/>
/// <reference path="../lib/phaser.d.ts"/>

import * as Phaser from 'phaser';
import {BootState} from './states/boot.ts';
import {PreloadState} from './states/preload.ts';
import {GameState} from './states/game.ts';

class Game extends Phaser.Game {

  constructor () {
    let width = 500;
    let height = 375;

    super(width, height, Phaser.AUTO, 'battlefield');
    //console.log(this, 'what is ME');
    
    this.state.add('Boot', BootState, false)
    this.state.add('Preload', PreloadState, false)
    this.state.add('Game', GameState, false)

    this.state.start('Boot');
  }
}

window.game = new Game();