import * as Phaser from 'phaser'
//import * as WebFont from 'webfontloader'

export class BootState extends Phaser.State {

  preload () {
    //assets we'll use in the loading screen
    this.load.image('preloadbar', 'assets/images/preloader-bar.png');
    /*WebFont.load({
      google: {
        families: ['Nunito']
      },
      active: this.fontsLoaded
    })*/
    /*
    let text = this.add.text(this.world.centerX, this.world.centerY, 'loading fonts', { font: '16px Arial', fill: '#dddddd', align: 'center' })
    text.anchor.setTo(0.5, 0.5)

    this.load.image('loaderBg', './assets/images/loader-bg.png')
    this.load.image('loaderBar', './assets/images/loader-bar.png')*/
  }

  create () {
    //loading screen will have a white background
    this.game.stage.backgroundColor = '#fff';

    //physics system
    this.game.physics.startSystem(Phaser.Physics.ARCADE);
  
    this.game.state.start('Preload');
  }

}
