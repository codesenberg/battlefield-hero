import * as Phaser from 'phaser';
import {Player} from '../components/player/player.ts';
import {BattlefieldObject} from '../components/battlefield-objects/battlefield-object.ts';

export class GameState extends Phaser.State {
  public wallpieces:IBattlefieldGroup;
  public fighters:Array<BattlefieldObject> = [];
  public testFloorTile:any;
  public graphics:any;
  public players:Array<Player> = [];
  public testFighterBad:any;
  public testFighterBad2:any;
  preload () {
    this.game.time.advancedTiming = true;
  }

  create () {
    var me = this.add.sprite(0, 0, 'battlefield-bg');
    this.testFloorTile =this.add.sprite(63, 63, 'battlefield-tile');
    this.physics.arcade.enable(this.testFloorTile);
    this.testFloorTile.inputEnabled = true;
    this.testFloorTile.events.onInputDown.add(this.listener, this);



    //this.add.sprite(0, 0, 'wallpiece');
    
    /* START OF STRAT OF ADDING TO SUB GROUP */
    //I create the parent group first. After I instantiate sub groups, I will add them to the necessary parent group
    //ie. rowGroup.add(subgroup) 
    var rowGroup1 = this.add.group();
    //rowGroup1.enableBody = true;
    //rowGroup1.physicsBodyType = Phaser.Physics.ARCADE;
    /* END OF STRAT OF ADDING TO SUB GROUP */


    /*
    this.wallpieces = <IBattlefieldGroup>this.add.group();
    this.wallpieces.enableBody = true;
    this.wallpieces.physicsBodyType = Phaser.Physics.ARCADE;
    this.wallpieces.battleFieldType = 'wall';

    var wallpieces2 = <IBattlefieldGroup>this.add.group();
    wallpieces2.enableBody = true;
    wallpieces2.physicsBodyType = Phaser.Physics.ARCADE;
    wallpieces2.battleFieldType = 'wall';*/
    
    /*var fighterGroup = <IBattlefieldGroup>this.add.group();
    fighterGroup.enableBody = true;
    fighterGroup.physicsBodyType = Phaser.Physics.ARCADE;
    fighterGroup.battleFieldType = 'fighter';

     var fighterGroup2 = <IBattlefieldGroup>this.add.group();
    fighterGroup2.enableBody = true;
    fighterGroup2.physicsBodyType = Phaser.Physics.ARCADE;
    fighterGroup2.battleFieldType = 'fighter';*/
    //var group2 = this.add.group();
   
    
    //console.log(this.cache.getImage("wallpiece").height, 'what me');
    //console.log(this.wallpieces.children,'what me height');
    //this.wallpiece.createMultiple(16, 'wallpiece');
    /*for (var i = 0; i < 8; i++)
    {
        //  This creates a new Phaser.Sprite instance within the group
        //  It will be randomly placed within the world and use the 'baddie' image to display
        this.wallpieces.create(0, this.cache.getImage("wallpiece").height * i, 'wallpiece');
    }*/
    //this.testFloorTile = new Phaser.Rectangle(63, 63, 63, 63);
    

    /*
   this.graphics = this.add.graphics(0, 0);
   
    // set a fill and line style
    this.graphics.lineStyle(5, 0x0000FF, 1);
    this.graphics.hitArea = new Phaser.Rectangle(63, 63, 63, 63)
    //console.log(this.graphics.hitArea, 'what is me?');
   var met = this.graphics.drawRect(63, 63, 63, 63);



    this.testFloorTile = this.add.sprite(0, 0)

    // Add the graphics to the sprite as a child
    this.testFloorTile.addChild(this.graphics);
    console.log(this.testFloorTile, 'WHAT NOW?')
    // Enable physics on the sprite (as graphics objects cannot have bodies applied)
    this.physics.arcade.enable(this.testFloorTile.children[0].hitArea);

    this.testFloorTile.inputEnabled = true;
    this.testFloorTile.events.onInputDown.add(this.listener, this);*/

    //this.physics.arcade.enable(this.graphics.hitArea);
     //this.graphics.inputEnabled = true;
    //this.graphics.events.onInputDown.add(this.listener, this);




    //START OF PLAYER CREATION// (MAYBE PUT IN SEPARATE STATE?)

    var player1 = new Player(0, this.game);
    this.players.push(player1);
    var player2 = new Player(1, this.game);
    this.players.push(player2);
    player1.init(player2.id, this.players);
    player2.init(player1.id, this.players);
    
    //END OF PLAYER CREATION//


    var fighterGroupA = this.add.group();
    fighterGroupA.enableBody = true;
    fighterGroupA.physicsBodyType = Phaser.Physics.ARCADE;
    this._createFighterGroup(fighterGroupA, this.game);

     //console.log(fighterGroupA, 'HI WORLD !!!!!');
    if (fighterGroupA.countDead())
    {
        let fighter = fighterGroupA.getFirstDead(false);
        fighter.reset(63, 63);

        //IMPORTANT, readd health after fighter reset!!!
        fighter.health = 60;
        fighter.config(player1.id, this.players[player1.enemyId]);
        player1.addFighter(fighter);
        this.fighters.push(fighter);
    }

    if (fighterGroupA.countDead())
    {
        let fighter = fighterGroupA.getFirstDead(false);
        fighter.reset(441, 63);

        //IMPORTANT, readd health after fighter reset!!!
        fighter.health = 60;
        fighter.config(player2.id, this.players[player2.enemyId]);
        player2.addFighter(fighter);
        this.fighters.push(fighter);
    }

    if (fighterGroupA.countDead())
    {
        let fighter = fighterGroupA.getFirstDead(false);
        fighter.reset(441, 63);

        //IMPORTANT, readd health after fighter reset!!!
        fighter.health = 60;
        fighter.config(player2.id, this.players[player2.enemyId]);
        player2.addFighter(fighter);
        this.fighters.push(fighter);
    }

      /*
      Legacy adding sprites to stage

      var testFighter = new BattlefieldObject(this.game, 63, 63, 'fighter1', player1.id, this.players[player1.enemyId]);
      var testFighterBad = new BattlefieldObject(this.game, 441, 63, 'fighter1', player2.id, this.players[player2.enemyId]);
      
      this.testFighterBad = testFighterBad;


      this.fighters.push(testFighter);
      player1.addFighter(testFighter);
      
      this.fighters.push(testFighterBad);
      player2.addFighter(testFighterBad);

      var testFighterBad2 = new BattlefieldObject(this.game, 441, 63, 'fighter1', player2.id, this.players[player2.enemyId]);
      this.fighters.push(testFighterBad2);
      player2.addFighter(testFighterBad2);

      */

      console.log(fighterGroupA, 'parent group AFTER ALL CREATION');


        setInterval(() => {
          console.log(fighterGroupA.countDead(), 'what is this count dead');
          //fighterGroupA.y = 100;
          /*
          testFighterBad2 = new Fighter('fighter1', 471, 63, this, player2.id, player2.enemyId);
          this.fighters.push(testFighterBad2);
          player2.addFighter(testFighterBad2.sprite);
          */
          //console.log(testFighter.sprite.renderOrderID, 'MAIN RENDER ID')
        },3000);
      //});

      //rowGroup1.add(testFighterBad2.sprite)      
      //player2.setEnemyFighters(player1.fighterGroup);

      
      setInterval(() =>{
        //fighterGroupA.children[0].kill(); //= false;
        /*let testFighter = new BattlefieldObject(this.game, 63, 63, 'fighter1', player1.id, this.players[player1.enemyId]);
        let testFighterBad = new BattlefieldObject(this.game, 441, 63, 'fighter1', player2.id, this.players[player2.enemyId]);
      
        this.fighters.push(testFighter);
        player1.addFighter(testFighter);
      
        this.fighters.push(testFighterBad);
        player2.addFighter(testFighterBad);*/
      }, 5000);

      setTimeout(() =>{
       //testFighterBad2.sprite.kill();
      // console.log('fighter 2 killed');
       //testFighter.sprite.body.velocity.x = 100;
      }, 3000);


    //console.log(this.game, 'what game');
    //window.testFire();
    //console.log(, 'what is now');
  }

  update () {
    for (let i = this.fighters.length - 1; i >= 0; i--) {
      //console.log(this.fighters[i].playerId, 'FIGHT ID');
      //console.log(this.players[this.fighters[i].playerId].getEnemyWall(), 'yo')

      //[this.testFighterBad.sprite, this.testFighterBad2.sprite]
       //this.fighters[i].update(this.players[this.fighters[i].enemyId].wallGroup, this.players[this.fighters[i].enemyId].fighterGroup);
        //Remove destroyed fighters
        if (this.fighters[i].body === null) {
          this.fighters.splice(i, 1);
        } else {
          this.fighters[i].update();
        }

      //this.fighters[i].update(this.wallpieces);
    }


    /*if (ammo !== undefined && !ammo.isHit) {
      ammo.body.velocity.x = 150;
      this.physics.arcade.collide(ammo, this.wallpieces, this.ammoHit, null, this);
    }*/
  }

  render () {
    this.game.debug.text(String(this.game.time.fps) || '--', 20, 70, "#00ff00", "40px Courier");
    //this.game.debug.geom(this.testFloorTile,'#0fffff'); 
  }

  ammoHit (ammo, target) {
    /*ammo.body.velocity.x = 0;
    ammo.isHit = true;
    target.onHit(target, ammo)
      console.log(target, 'my TARG');
      */
      /*player.animations.stop();
      badGuy.animations.stop();*/
  }

  listener () {
    
    var me = this.physics.arcade.overlap(this.testFloorTile,   this.fighters[0]);
    if (!me) {
      //console.log(this.testFloorTile, 'my testFloorTile');
      //var testFighter = new Fighter('fighter1', this.testFloorTile.x, this.testFloorTile.y, this);
      //this.fighters.push(testFighter);
    } else {
      //console.log('cant be added');
    }
    //console.log(me, 'YEOSH!!')
  }

  //Simulates create a pool of 20 for one type of fighter class; this would be triggered for all potential/available fight classes
  //For battle mode, all pools will be created; If story mode, only the available fighters that are needed at that time will have those pools created for them
  //IMPORTANT, there needs to be a test done to ensure that 20 is a good number. Once game is created
  //completed, benchmarks can be performed by trying to create the same fighter object as fast of possible within the context of the game
  //to see if 20 is a suffice number
  private _createFighterGroup (fighterGroupA, game) {
      let aFighters = []
      for (let i = 0; i < 20; i ++ ) {
        let fighter = new BattlefieldObject(game, 0, 0, 'fighter1');
            fighter.alive = false;
            fighter.exists = false;
            aFighters.push(fighter);
      }
      //console.log(ammo, 'my ammo!')
      fighterGroupA.addMultiple(aFighters, false);
  }
}
