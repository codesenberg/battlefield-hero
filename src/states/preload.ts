import * as Phaser from 'phaser';

export class PreloadState extends Phaser.State {
  private preloadBar:any;

  preload () {
    //show loading screen
    this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'preloadbar');
    this.preloadBar.anchor.setTo(0.5);
    this.preloadBar.scale.setTo(3);

    this.load.setPreloadSprite(this.preloadBar);

    //load game assets
    this.load.image('battlefield-bg', 'assets/images/battlefield-bg-overlay.png');
    this.load.image('wallpiece', 'assets/images/wallpiece.png');
    this.load.image('ammo', 'assets/images/ammo.png');
    this.load.image('fighter1', 'assets/images/fighter1.png');
    this.load.image('battlefield-tile', 'assets/images/battlefield-tile.png');
  }

  create () {
    this.game.state.start('Game')
  }

}
