export namespace GameUtils {
	let spriteIdCounter = 0;
	
    export function isPlayer1(playerId:number, debug?:boolean):boolean {
    	if (debug) {
    		//console.log('id that is checked:', playerId)
    	}
    	return (playerId === 0);
    }

    export function createSpriteId () {
    	return spriteIdCounter++;
    }
}